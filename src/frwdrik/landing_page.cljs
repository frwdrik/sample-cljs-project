(ns ^:figwheel-hooks frwdrik.landing-page
  (:require
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom]))


(defn welcome-message []
  [:section {:class "section"}
   [:h1 {:class "title"}
    "Welcome to this place"]
   [:div {:class "content"}
    [:p "Butter beans in green soups are too few in bad company"]]
   [:a {:href "/dd"} "Go play Dice of Doom!!"]])

(defn landing-page []
  [welcome-message])

(defn hello-world []
  [:div
   [:h3 "Edit this! in src/frwdrik/landing_page.cljs and watch it change before your eyes!"]])

;;; Mounting
(defn multiply [x y]
  (* x y))

(defn get-app-element []
  (gdom/getElement "app"))

(defn mount [el]
  (rdom/render [landing-page] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
